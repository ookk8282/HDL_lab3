module lab3_2(clk, reset, sel_clk, LED);
    input clk, reset, sel_clk;
    output [15:0] LED;

    wire [7:0] light, reverse_light;
    wire light_done;
    wire clk19, clk26, curruent_clk;
    reg [7:0] left_LED, right_LED;
    reg flag;
    wire next_flag;

    light_motion lm( .clk(curruent_clk)
                   , .reset(reset)
                   , .light(light)
                   , .back()
                   , .done(light_done));
    reverse r8(light, reverse_light);
    // clock
    clock_divider_19_26 clk_div(clk, clk19, clk26);
    assign curruent_clk = sel_clk ? clk19 : clk26;

    // determine shine on left part(0) or right part(1)
    always@(posedge curruent_clk or posedge reset) begin
        flag <= reset ? 1'b0 : next_flag;
    end
    // finish a cycle, change to another part
    assign next_flag = light_done ? ~flag : flag;

    assign LED = {left_LED, right_LED};
    always@(*) begin
        // light shine on left part
        left_LED  <= light;
        right_LED <= 0;
        if (flag) begin
            // right part
            left_LED  <= 0;
            right_LED <= reverse_light;
        end
    end
endmodule
