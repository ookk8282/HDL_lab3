`timescale 1ns / 1ps

module test_light_motion;
    reg reset, clk;
    wire [7:0] light;

    light_motion lm(reset, clk, light, back, done);

    initial begin
        $monitor("%5dns: reset = %b, light = %b, back = %b, done = %b",
            $time, reset, light, back, done);
        clk = 1'b0;
        reset = 1'b1;
        #5
        reset = 0;
        #315

        $display("----- done ------");
        $finish;
    end

    always begin
        #5
        clk = ~clk;
    end
endmodule
