module clock_divider_19_26(clk, clk19, clk26);
    input clk;
    output clk19, clk26;

    reg  [25:0]count = 0;
    wire [25:0]next_count;

    always@(posedge clk) begin
        count <= next_count;
    end

    assign next_count = count + 1;
    assign clk26 = count[25];
    assign clk19 = count[18];
endmodule
