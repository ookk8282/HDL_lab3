module test_lab3_1;
    reg clk, reset;
    wire [15:0] LED;

    lab3_1 lab(clk, reset, LED);

    initial begin
        $monitor("%4dns: reset = %b, LED = %b", $time, reset, LED);
        clk = 1'b0;
        reset = 1'b1;
        #130
        reset = 1'b0;
        #26000;
        $finish;
    end

    always begin
        #5
        clk = ~clk;
    end
endmodule
