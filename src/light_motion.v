module light_motion(reset, clk, light, back, done);
    input reset, clk;
    output reg back, done;
    output reg [7:0] light;

    reg dir;
    wire [7:0] next_light;

    always @(posedge clk or posedge reset) begin
        light <= reset ? 0 : next_light;
        dir   <= reset ? 0 : next_dir;
        back  <= reset ? 0 : next_back;
        done  <= reset ? 0 : next_done;
    end

    light_step ls(light, dir, next_light);

    wire next_back = dir == 1'b0 && light == 8'b0111_1111;
    wire next_done = dir == 1'b1 && light == 8'b0000_0001;
    wire next_dir = (next_back || next_done) ? ~dir : dir;
endmodule

module light_step(in, dir, out);
    input [7:0] in;
    input dir;
    output [7:0] out;

    assign out = dir ? {1'b0, in[7:1]}
                     : {in[6:0], 1'b1};
endmodule
