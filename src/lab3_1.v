module lab3_1(clk, reset, LED);
    input clk, reset;
    output [15:0] LED;

    wire [7:0] light, reverse_light;
    wire light_done;
    reg [7:0] left_LED, right_LED;
    wire clk26;
    reg flag; // determine shine on left part(0) or right part(1)

    light_motion lm( .clk(clk26)
                   , .reset(reset)
                   , .light(light)
                   , .back()
                   , .done(light_done));
    clock_divider_26 clk_div_26(clk, clk26);
    reverse r8(light, reverse_light);

    assign LED = {left_LED, right_LED};
    always@(*) begin
        // light shine on left part
        left_LED  <= light;
        right_LED <= 0;
        if (flag) begin
            // right part
            left_LED  <= 0;
            right_LED <= reverse_light;
        end
    end
    // finish a cycle, change to another part
    always@(posedge clk26 or posedge reset) begin
        flag <= reset ? 1'b0 : next_flag;
    end
    wire next_flag = light_done ? ~flag : flag;
endmodule
