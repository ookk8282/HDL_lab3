module reverse(in, out);
    parameter width = 8;

    input  [width-1:0] in;
    output [width-1:0] out;
    reg [width-1:0] out;

    integer i;
    always@(in) begin
        for (i=0; i<width; i=i+1)
            out[i] = in[width-1-i];
    end
endmodule
